<?php
include 'xyz/config.php';

if(!isset($_SESSION['user'])){
    require __DIR__ ."/ui/login.php";
    exit;   
}

$request = $_SERVER['REQUEST_URI'];

$request = explode("/",$request);

$params = null;
if(isset($request[2])){
	$params = $request[2];
}else{
	$params = $request[1];
}

$params = explode("?",$params);

if(isset($params[1])){
    $_gets = $params[1];
    $_gets = explode("&",$_gets);
    foreach($_gets as $vall ){
        $vall = explode("=",$vall);
        if(count($vall) == 2){
           $_get[$vall[0]] =  $vall[1];
        }
    }
}

$params = $params[0];
$_event_id =  $_SESSION['user']['Event'];



switch ($params) {
    case '' :
        header("Location: xyz/dashboard");
        break;
	case 'dashboard' :
        require __DIR__ . '/ui/dashboard.php';
        break;
    case 'transaction' :
        require __DIR__ . '/ui/transaction.php';
        break;
	case 'callbackbjb' :
        require __DIR__ . '/ui/callbackbjb.php';
        break;
	case 'couponstransaction' :
        require __DIR__ . '/ui/couponstransaction.php';
        break;
	case 'profileparticipant' :
        require __DIR__ . '/ui/profileparticipant.php';
        break;
    case 'coupons' :
        require __DIR__ . '/ui/coupons.php';
        break;
    case 'import' :
        require __DIR__ . '/ui/import.php';
        break;
	case 'ticket' :
        require __DIR__ . '/ui/ticket.php';
        break;
    case 'etiket' :
        require __DIR__ . '/ui/etiket.php';
        break;
    case 'runner' :
        require __DIR__ . '/ui/runner.php';
        break;
    case 'courier' :
        require __DIR__ . '/ui/courier.php';
        break;
    case 'report' :
        require __DIR__ . '/ui/report.php';
        break;
    case 'runnerreport' :
        require __DIR__ . '/ui/runnerreport.php';
        break;
    case 'timelinereportvr' :
        require __DIR__ . '/ui/timelinereportvr.php';
        break;
    case 'invite' :
        require __DIR__ . '/ui/invite.php';
        break;
	case 'ticketxyz' :
        require __DIR__ . '/ui/ticketxyz.php';
        break;
    case 'timeline' :
        require __DIR__ . '/ui/timeline.php';
        break;
    case 'timelineresi' :
        require __DIR__ . '/ui/timelineresi.php';
        break;
	case 'resend' :
        require __DIR__ . '/xyz/resend.php';
        break;
    case 'resetvr' :
        require __DIR__ . '/xyz/resetvr.php';
        break;        
    case 'garmindelete' :
        require __DIR__ . '/xyz/garmin.php';
        break;
    case 'confrimvr' :
        require __DIR__ . '/xyz/confrimvr.php';
        break;
    case 'unconfrimvr' :
        require __DIR__ . '/xyz/unconfrimvr.php';
        break;
    case 'event' :
        if(isset($request[3])) change_event($request[3]);
        require __DIR__ . '/xyz/event.php';
        break;
    default:
        break;
}


function change_event($event) {
    if((int)$event <> 0 ) {
        foreach($_SESSION['user']['Events'] as $vall){
            if($vall->evnhId == $event){
                $_SESSION['user']['Event']      = $vall->evnhId;
                $_SESSION['user']['EventName']  = $vall->evnhName;
            }
        }
    }
}

?>