<?php
include 'config.php';

    // ADD
    if(isset($_POST['username'])  AND isset($_POST['password'])){
        
        $user = $_POST['username'];
        $pass = $_POST['password'];
      
        $hashed_string['username'] = $user;
        $hashed_string['password'] = $pass;
        
        $data_post = array(
            'data' => $hashed_string,
        );
   
		$response = get_content_login($url.'/api/v1/login', json_encode($data_post));
        
        unset($_POST['username']);
        unset($_POST['password']);
        
		
        $response = json_decode($response);
        

		if(isset($response->status->error->message)){
            header('Location: ' ."login.php?error=".$response->status->error->message);
            exit;
		}else{

			$event[1] = 'Bank Bjb';
			
			$_SESSION['user']['UserName']   = $response->data->userName;
			$_SESSION['user']['FullName']   = $response->data->userFullName;
            $_SESSION['user']['Role']       = "Admin";// $row['authRole'];
            $_SESSION['user']['Events']     = $event;
			$_SESSION['user']['Event']      = 1;
			$_SESSION['user']['EventName']	= 'Bank Bjb';
            
            
            header("Location: transaction");
            exit;
		}
		
    }

?>
