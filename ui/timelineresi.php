<?php 
include __DIR__.'/../xyz/menu.php';

	

    $invoice = "n/a";
    if(isset($request[3])){
		$tmp = explode("?",$request[3]);
		$request[3] = $tmp[0];
		if(isset($tmp[1])){
			$resend_msg =  $tmp[1];
		}
		
        $invoice = $request[3];
    }



    // GET DATA
    $ch = curl_init(); 
                    
                    
    $url_ = $titu."api/v1/cekresi_vr/".$invoice;
                    
              

    // set url
    curl_setopt($ch, CURLOPT_URL, $url_);

    // return the transfer as a string 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                

    // $output contains the output string 
    $output = curl_exec($ch); 
    
    // tutup curl 
    curl_close($ch);      

    // menampilkan hasil curl
    $data_all = json_decode($output);

?>



<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Steelytoe Xyz</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>Xyz</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Steelytoe</b>Xyz</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="hidden-xs"> <?php echo $_SESSION['user']['EventName']; ?> &nbsp; </span>
              <i class="fa fa-calendar"> </i>
              <span class="label label-success"><?php echo COUNT($_SESSION['user']['Events']); ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <?php echo COUNT($_SESSION['user']['Events']); ?> events</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                <?php
                    foreach($_SESSION['user']['Events'] AS $vall){
                        echo "<li><a href='".'../../xyz/event/'.$vall->evnhId."'><h3>".$vall->evnhName."</i></h3></a></li>";
                    }
                  
                ?>
 
                </ul>
              </li>
              <li class="footer"><a href="#">Close</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- Tasks: style can be found in dropdown.less -->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="../../xyz/logout.php" class="dropdown-toggle" >
             
               <span class="hidden-xs">Sign Out</span>
				<i class="fa fa-sign-out"> </i>
            </a>

              
          <!-- Control Sidebar Toggle Button -->

        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <?php
    

    $menu = str_replace("{{runner}}","class='active'",$menu);
    echo $menu;
  
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Timeline Resi 
        <small>#<?php if(isset($data_all->data->result->summary->waybill_number))echo $data_all->data->result->summary->waybill_number; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="../dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="../transaction">Runner</a></li>
        <li class="active">Timeline Resi</li>
      </ol>
    </section>

	<!--
    <div class="pad margin no-print" >
      <div class="callout callout-success" style="margin-bottom: 0!important;">
        <h4><i class="icon fa fa-check"></i> Status : Confirmed</h4>
        
      </div>
    </div> 
	
	
	  
-->

	<?php 
		if(isset($resend_msg)){
			
			$resend_msg = explode("-",$resend_msg);
			$resend_msg[0] = str_replace("msg=","",$resend_msg[0]);
		
			
			foreach($data_all->linked->lgntTrnsId as $vall){
				if($resend_msg[0] == $vall->lgntId){
					$resend_msg[0] = str_replace("_"," ",$vall->lgntTypeName)." to ".$vall->lgntTargetEmail;
				}
			
			}
			
			
			if($resend_msg[1] == "SEND"){
				echo "<div class='pad margin no-print' >";
					echo "<div class='alert alert-success alert-dismissible'>";
						echo "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>";
						echo "<h4><i class='icon fa fa-check'></i> Send Email Successfully!</h4>";
						echo "Success send email ".$resend_msg[0];
					echo "</div>";
				 echo "</div>";
			}else{
				echo "<div class='pad margin no-print' >";
					echo "<div class='alert alert-danger alert-dismissible'>";
						echo "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>";
						echo "<h4><i class='icon fa fa-ban'></i> Failed To Send Email!</h4>";
						echo "Failed send email ".$resend_msg[0];
					echo "</div>";
				 echo "</div>";
			}
			
			
			
		}
	?>
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <strong> SUMMARY </strong>: <?php if(isset($data_all->data->result->summary->waybill_number))echo $data_all->data->result->summary->waybill_number; ?><strong>(<?php if(isset($data_all->data->result->summary->status))echo $data_all->data->result->summary->status; ?>)</strong>
           
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">

        <!-- /.col -->

        <div class="col-sm-6 invoice-col">
            Courier Code : <strong><?php if(isset($data_all->data->result->summary->courier_code))echo $data_all->data->result->summary->courier_code; ?></strong></br>
            Courier Name : <b><?php if(isset($data_all->data->result->summary->courier_name))echo $data_all->data->result->summary->courier_name; ?></b><br>
            Waybill Number :<b> <?php if(isset($data_all->data->result->summary->waybill_number))echo $data_all->data->result->summary->waybill_number; ?></b><br>
            Service Code : <strong><?php if(isset($data_all->data->result->summary->service_code))echo $data_all->data->result->summary->service_code; ?></strong></br>
            Waybill Date : <b><?php if(isset($data_all->data->result->summary->waybill_date))echo $data_all->data->result->summary->waybill_date; ?></b><br>
            Status :<b> <?php if(isset($data_all->data->result->summary->status))echo $data_all->data->result->summary->status; ?></b>
  
  
  
  
        </div>
        <!-- /.col -->
        <div class="col-sm-6 invoice-col">
          Shippper Name : <strong><?php if(isset($data_all->data->result->details->shippper_name))echo $data_all->data->result->details->shippper_name; ?></strong></br>
          Shipper City : <b><?php if(isset($data_all->data->result->details->shipper_city))echo $data_all->data->result->details->shipper_city; ?></b><br>
          Origin :<b> <?php if(isset($data_all->data->result->details->origin))echo $data_all->data->result->details->origin; ?></b><br>
  
          Receiver Name : <b><?php if(isset($data_all->data->result->details->receiver_name))echo $data_all->data->result->details->receiver_name; ?></b><br>
          Receiver City : <b><?php if(isset($data_all->data->result->details->receiver_city))echo $data_all->data->result->details->receiver_city; ?></b> <br>
          Destination :   <b><?php if(isset($data_all->data->result->details->destination))echo $data_all->data->result->details->destination; ?></b><br>
        </div>

        <!-- /.col -->
      </div>
      <!-- /.row -->
    
    <h3>Delivery Status</h3>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Status</th>
              <th>POD Receiver</th>
              <th>POD Date</th>
              <th>POD Time</th>
            </tr>
            </thead>
            <tbody>
            
            <?php 
            
                if(isset($data_all->data->result->delivery_status)){
					
						echo "<tr>";
						  echo "<td>".$data_all->data->result->delivery_status->status."</td>";
						  echo "<td>".$data_all->data->result->delivery_status->pod_receiver."</td>";
						  echo "<td>".$data_all->data->result->delivery_status->pod_date."</td>";
                          echo "<td>".$data_all->data->result->delivery_status->pod_time."</td>";
						echo "</tr>";
					
				
				}
            
            ?>
            
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      
      
      

    <h3>Manifest Data</h3>
    <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Manifest Date</th>
              <th>Manifest Time</th>
              <th>City Name</th>
              <th>Manifest Description</th>
            </tr>
            </thead>
            <tbody>
            
            <?php 
            
                if(isset($data_all->data->result->manifest)){

					foreach($data_all->data->result->manifest as $vall){
						echo "<tr>";
								
							echo "<td>".$vall->manifest_date."</td>";
							echo "<td>".$vall->manifest_time."</td>";
							echo "<td>".$vall->city_name."</td>";
							echo "<td>".$vall->manifest_description."</td>";
						 
						echo "</tr>";
					}
				
				}
            
            ?>
            
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      

      <!-- this row will not appear when printing -->
        <div class="row no-print">
        <div class="col-xs-12">
          <a href="../runner"  class="btn btn-default"><i class=""></i> Back</a>
         <!--  
         <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
            <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
          </button> 
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button> -->
        </div>
      </div> 
    </section> 
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.13
    </div>
    <strong>Copyright &copy; 2019 <a href="#">SteelytoeXyz</a>.</strong> All rights
    reserved.
  </footer>


  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>
